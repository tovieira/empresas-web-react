import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import Navbar from './components/layout/Navbar/Navbar'
import Alert from './components/layout/Alert/Alert'
import Home from './components/pages/Home/Home'
import Company from './components/companies/Company/Company'
import Login from './components/auth/Login'
import PrivateRoute from './components/routing/PrivateRoute'
import './components/assets/App.css'

import CompaniesState from './context/companies/CompaniesState'
import AuthState from './context/auth/AuthState'
import AlertState from './context/alert/AlertSate'

const App = () => {
    return (
        <AuthState>
            <CompaniesState>
                <AlertState>
                    <Router>
                        <div className='App'>
                            <Navbar />
                            <div className='container'>
                                <Switch>
                                    <PrivateRoute exact path='/' component={Home} />
                                    <Route exact path='/login' component={Login} />
                                    <Route exact path='/company/:id' component={Company} />
                                </Switch>
                                <Alert />
                            </div>
                        </div>
                    </Router>
                </AlertState>
            </CompaniesState>
        </AuthState>
    );
}

export default App;
