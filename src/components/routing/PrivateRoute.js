import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import { Route, Redirect } from 'react-router-dom';
import AuthContext from '../../context/auth/authContext'

const PrivateRoute = ({ component: Component, ...rest }) => {
    const authContext = useContext(AuthContext);

    const { isAuthenticated, loading } = authContext

    return (
        <Route {...rest} render={props => !isAuthenticated && !loading ? (
            <Redirect to='/login' />
        ) : (
                <Component {...props} />
            )} />
    )
}

PrivateRoute.propTypes = {
    component: PropTypes.object.isRequired
}

export default PrivateRoute
