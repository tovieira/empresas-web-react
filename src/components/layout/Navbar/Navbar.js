import React, { useContext, Fragment } from 'react'
import AuthContext from '../../../context/auth/authContext'
import styled from 'styled-components'
import logo from '../../assets/logoNav.png';
import './Navbar.css'

const Nav = styled.nav`
    display: flex;
    flex-direction: row-reverse;
    justify-content: start;
    align-items: center;
    padding: 0.7rem 2rem;
    z-index: 1;
    width: 100%;
    height: 3rem;
    border-bottom: solid 1px #f4f4f4;
    opacity: 0.9;
    margin-bottom: 1rem;

    background: rgb(238, 76, 119);
    background: linear-gradient(
        180deg,
        rgba(238, 76, 119, 1) 0%,
        rgba(187, 17, 62, 1) 100%
    );

    img {
        max-width: 5rem;
        position: absolute;
        left: 50%;
        transform: translateX(-50%)
    }

    button {
        justify-self: flex-start;

        display: inline-block;
        background: #f4f4f4;
        color: #333;
        padding: 0.2rem 1rem;
        font-size: 1rem;
        letter-spacing: normal;
    text-align: center;
        border: none;
        cursor: pointer;
        margin-right: 0.5rem;
        transition: opacity 0.2s ease-in;
        outline: none;
        border-radius: 1.8px;
    }
`

const Navbar = () => {
    const authContext = useContext(AuthContext);

    const { isAuthenticated, logoutUser } = authContext

    const onLogout = () => {
        logoutUser()
    }

    const authBar = (
        <Fragment>
            <Nav>
                <img src={logo} alt='logo iOasys' />
                <button onClick={onLogout}>Sair</button>
            </Nav>
        </Fragment>
    )

    const guestBar = (
        <Fragment>
            <Nav>
                <img src={logo} alt='logo iOasys' />
            </Nav>
        </Fragment>
    )

    return (
        <Fragment>
            {isAuthenticated ? authBar : guestBar}
        </Fragment>
    )
}

export default Navbar
