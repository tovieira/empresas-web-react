import React, { Fragment } from 'react'
import Search from '../../companies/Search/Search'
import Companies from '../../companies/Companies/Companies'
import './Home.css'

const Home = () => {
    return (
        <Fragment>
            <Search />
            <Companies />
        </Fragment>
    )
}

export default Home
