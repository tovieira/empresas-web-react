import React, { useState, useContext, useEffect } from 'react'
import PropTypes from 'prop-types'
import AlertContext from '../../context/alert/alertContext'
import AuthContext from '../../context/auth/authContext'
import logo from '../assets/logoHome.png';
import emailIcon from '../assets/mail-outlined.svg'
import lockIcon from '../assets/bx-lock-open-alt.svg'
import styled from 'styled-components'

const FormLogin = styled.div`
    display:flex;
    flex-direction: column;
    align-items: center;

    max-width: 500px;
    margin: 2rem auto;
    overflow: hidden;
    padding: 0 2rem;

    .logo {
        width: 80%;
        margin-bottom: 1.5rem
    }

    h1 {
        font-size: 2rem;
        margin-bottom: 1.5rem
    }

    p {
        font-size: 1.25rem
    }

    form {
        width: 100%;

        .form-group {
            margin: 1.2rem 0;
            position: relative;
        }

        label {
            display: none;
        }

        .emailIcon, .lockIcon {
            width: 1rem;
            position: relative;
            top: 2.25em;
            left: 0.5rem;
        }

        input {
            display: block;
            width: 100%;
            padding: 0.4rem;
            padding-left: 2.5rem;
            font-size: 1.2rem;
            background-color: transparent;
            border: none;
            border-bottom: 2px solid #333;
            transition: all ease 0.3s;
            margin: 0.2rem 0;


            &:focus {
                border-bottom: 2px solid #ee4c77;
                outline: none;
            }
        }
    }

    button {
        display: block;
        width: 100%;
        padding: 0.5rem 0;
        border-radius: 1.8px;
        border: none;
        background-color: #57bbbc;
        letter-spacing: normal;
        text-align: center;
        color: #ffffff;

        &:focus {
            outline: none;
        }
    }
`

const Login = (props) => {
    const alertContext = useContext(AlertContext)
    const authContext = useContext(AuthContext)

    const { setAlert } = alertContext;

    const { loginUser, error, clearErrors, isAuthenticated } = authContext;

    useEffect(() => {
        if (isAuthenticated) {
            props.history.push('/')
        }

        if (error === 'Invalid login credentials. Please try again.') {
            setAlert('Dados de login invalidos. Por favor tente novamente.', 'danger');
            clearErrors();
        }
        //eslint-disable-next-line
    }, [error, isAuthenticated, props.history]);

    const [user, setUser] = useState({
        email: '',
        password: '',
    })

    const { email, password } = user;

    const onChange = (e) => setUser({ ...user, [e.target.name]: e.target.value });

    const onSubmit = (e) => {
        e.preventDefault();
        if (email === '' || password === '') {
            setAlert('Por favor preencha todos os campos', 'danger');
        } else {
            loginUser({
                email,
                password
            })
        }
    }

    return (
        <FormLogin>
            <img src={logo} alt='logo iOasys' className='logo' />
            <h1>BEM-VINDO AO EMPRESAS</h1>
            <p>Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.</p>
            <form onSubmit={onSubmit}>
                <div className='form-group'>
                    <label htmlFor='email'>Email</label>
                    <img src={emailIcon} className='emailIcon' alt='ícone email' />
                    <input type='email' name='email' value={email} onChange={onChange} required />
                </div>
                <div className='form-group'>
                    <label htmlFor='password'>Senha</label>
                    <img src={lockIcon} className='lockIcon' alt='ícone cadeado' />
                    <input type='password' name='password' value={password} onChange={onChange} required />
                </div>
                <button type='submit' className='btn-block'>ENTRAR</button>
            </form>
        </FormLogin>
    )
}

Login.propTypes = {
    history: PropTypes.object.isRequired
}

export default Login