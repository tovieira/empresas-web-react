import React, { useContext } from 'react'
import CompanyItem from '../CompanyItem/CompanyItem'
import Spinner from '../../layout/Spinner'
import CompaniesContext from '../../../context/companies/companiesContext'

const Companies = () => {
    const companiesContext = useContext(CompaniesContext)

    const { loading, companies } = companiesContext;

    if (loading) {
        return <Spinner />
    } else {
        return (
            <div className='grid-2'>
                {companies.map(companie => (
                    <CompanyItem key={companie.id} companieProp={companie} />
                ))}
            </div>
        )
    }
}

export default Companies
