import React, { Fragment, useEffect, useContext } from 'react'
import PropTypes from 'prop-types'
import Spinner from '../../layout/Spinner'
import { Link } from 'react-router-dom'
import CompaniesContext from '../../../context/companies/companiesContext'
import styled from 'styled-components'

const CompanyCard = styled.div`
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    grid-gap: 2rem;
    padding: 1rem;
    border: #ccc 1px dotted;
    margin: 0.7rem 0;
    background-color: var(--white-two);

    @media (max-width: 700px)    {
        grid-template-columns: 1fr;
    }

    img {
        width: 100%;       
    }

    p {
        font-size:1rem;
        font-family: Roboto;
        font-weight: normal;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.44;
        letter-spacing: auto;
        text-align: left;
        color: #333;
    }
`

const Company = ({ match }) => {
    const companiesContext = useContext(CompaniesContext)

    const { companiesShow, loading, companie } = companiesContext;

    useEffect(() => {
        companiesShow(match.params.id)
    }, [match.params.id])

    const { enterprise_name, description } = companie

    if (loading) return <Spinner />

    return (
        <Fragment>
            <Link to='/' className="btn btn-light">Voltar para busca</Link>
            <CompanyCard>
                <img src='https://source.unsplash.com/collection/1608456/640x480' alt={enterprise_name} style={{ width: '100%' }} />
                <p>{description}</p>
            </CompanyCard>
        </Fragment>
    )
}

Company.propTypes = {
    match: PropTypes.object.isRequired
}

export default Company
