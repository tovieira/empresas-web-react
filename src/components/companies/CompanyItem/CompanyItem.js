import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

const CompanyCard = styled.div`
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    grid-gap: 2rem;

    @media (max-width: 700px) {
        grid-template-columns: 1fr;
    }

    padding: 1rem;
    border: #ccc 1px dotted;
    margin: 0.7rem 0;
    background-color: #fff;

    h3, h4, h5 {
        text-align: left;
        font-family: 'Roboto', sans-serif;
    }
`

const CompanyItem = ({ companieProp: { id, enterprise_name, country, enterprise_type } }) => {

    const { enterprise_type_name } = enterprise_type;

    return (
        <CompanyCard>
            <img src='https://source.unsplash.com/collection/1608456/640x480' alt={enterprise_name} />
            <div>
                <h3>{enterprise_name}</h3>
                <h4>{enterprise_type_name}</h4>
                <h5>{country}</h5>
                <div>
                    <Link to={`/company/${id}`} className='btn btn-sm my-1'>Ver detalhes</Link>
                </div>
            </div>
        </CompanyCard>
    )
}

CompanyItem.propTypes = {
    companieProp: PropTypes.object.isRequired
}

export default CompanyItem
