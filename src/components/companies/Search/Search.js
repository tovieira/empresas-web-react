import React, { useContext, useState } from 'react'
import CompaniesContext from '../../../context/companies/companiesContext'
import AlertContext from '../../../context/alert/alertContext'
import companiesTypes from './companiesTypes'

const Search = () => {
    const companiesContext = useContext(CompaniesContext)
    const alertContext = useContext(AlertContext)

    const [text, setText] = useState('');
    const [select, setSelect] = useState([]);

    const onSubmit = (e) => {
        e.preventDefault();

        if (text === '') {
            alertContext.setAlert('Por favor insira o nome de uma empresa', 'light');
        } else {
            companiesContext.companiesFilter(text, select);
        }
    }

    const onChange = (e) => setText(e.target.value)
    const onSelect = (e) => setSelect(e.target.value)

    return (
        <div>
            <form onSubmit={onSubmit} className='form'>
                <label>
                    Escolha o setor da empresa
                    <select onChange={onSelect} placeholder="Selecione um tipo de empresa...">
                        {Object.entries(companiesTypes).map(([key, value]) => <option key={key} value={key}>{value}</option>)}
                    </select>
                </label>
                <input type='text' name='text' placeholder='Escreva o nome de uma empresa...' value={text} onChange={onChange} />
                <input type='submit' value='Pesquisar' className='btn btn-dark btn-block' />
            </form>
            {companiesContext.companies.length > 0 && (
                <button className='btn btn-light btn-block' onClick={companiesContext.clearCompanies}>Limpar pesquisa</button>
            )}
        </div>
    )
}

export default Search
