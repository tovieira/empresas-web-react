import {
    SET_LOADING,
    UNSET_LOADING,
    COMPANIES_INDEX,
    COMPANIES_SHOW,
    COMPANIES_FILTER,
    CLEAR_COMPANIES
} from '../types'

export default (state, action) => {
    switch (action.type) {
        case SET_LOADING:
            return {
                ...state,
                loading: true
            }
        case UNSET_LOADING:
            return {
                ...state,
                loading: false
            }
        case COMPANIES_INDEX:
            return {
                ...state,
                companies: action.payload,
                loading: false
            }
        case COMPANIES_FILTER:
            return {
                ...state,
                companies: action.payload,
                loading: false
            }
        case COMPANIES_SHOW:
            return {
                ...state,
                companie: action.payload,
                loading: false
            }
        case CLEAR_COMPANIES:
            return {
                ...state,
                companies: [],
                loading: false
            }
        default:
            return state;
    }
}