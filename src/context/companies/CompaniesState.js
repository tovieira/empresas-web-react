import React, { useReducer } from 'react'
import PropTypes from 'prop-types'
import axios from 'axios'
import CompaniesContext from './companiesContext'
import CompaniesReducer from './companiesReducer'

import {
    SET_LOADING,
    UNSET_LOADING,
    COMPANIES_INDEX,
    COMPANIES_SHOW,
    COMPANIES_FILTER,
    CLEAR_COMPANIES,
    AUTH_ERROR,
} from '../types'

let companiesAccesToken = localStorage.getItem('token');
let companiesClient = localStorage.getItem('client');
let companiesUid = localStorage.getItem('uid');

const CompaniesState = (props) => {
    const initialState = {
        companies: [],
        companie: {},
        loading: false
    }

    const [state, dispatch] = useReducer(CompaniesReducer, initialState);

    // Get all companies
    const companiesIndex = async () => {
        setLoading();

        const config = {
            headers: {
                'access-token': companiesAccesToken,
                'client': companiesClient,
                'uid': companiesUid,
            }
        }

        try {
            const companiesResponse = await axios.get('https://empresas.ioasys.com.br/api/v1/enterprises', config)

            dispatch({
                type: COMPANIES_INDEX,
                payload: companiesResponse.data.enterprises
            })

        } catch (err) {
            alert(`${err}`, 'light');

        }
    }

    // Get filtered companies
    const companiesFilter = async (enterprise_name, enterprise_type) => {
        setLoading();

        const config = {
            headers: {
                'access-token': companiesAccesToken,
                'client': companiesClient,
                'uid': companiesUid,
            }
        }

        try {
            const companiesResponse = await axios.get(`https://cors-anywhere.herokuapp.com/https://empresas.ioasys.com.br/api/v1/enterprises?enterprise_types=${enterprise_type}&name=${enterprise_name}`, config)

            dispatch({
                type: COMPANIES_FILTER,
                payload: companiesResponse.data.enterprises
            })

        } catch (err) {
            if (err.response.data.errors[0] === 'You need to sign in or sign up before continuing.') {
                alert('Sua sessão expirou. Clique em Logout e faça login novamente.');
                dispatch({
                    type: AUTH_ERROR,
                })
                unsetLoading()
            } else {
                alert(err)
                unsetLoading()
            }
        }
    }

    // Get single companie
    const companiesShow = async (enterprise_id) => {
        setLoading();

        const config = {
            headers: {
                'access-token': companiesAccesToken,
                'client': companiesClient,
                'uid': companiesUid,
            }
        }

        try {
            const companiesResponse = await axios.get(`https://cors-anywhere.herokuapp.com/https://empresas.ioasys.com.br/api/v1/enterprises/${enterprise_id}`, config)

            dispatch({
                type: COMPANIES_SHOW,
                payload: companiesResponse.data.enterprise
            })

        } catch (err) {
            alert(err.response.data.errors[0])
        }
    }

    // Clear companies
    const clearCompanies = () => dispatch({ type: CLEAR_COMPANIES })

    // Set Loading
    const setLoading = () => dispatch({ type: SET_LOADING })

    // Unset loading
    const unsetLoading = () => dispatch({ type: UNSET_LOADING })

    return <CompaniesContext.Provider
        value={{
            companies: state.companies,
            companie: state.companie,
            loading: state.loading,
            companiesIndex,
            companiesFilter,
            companiesShow,
            clearCompanies
        }}
    >
        {props.children}
    </CompaniesContext.Provider>
}

CompaniesState.propTypes = {
    children: PropTypes.object.isRequired
}

export default CompaniesState