import { createContext } from 'react';

const companiesContext = createContext();

export default companiesContext;