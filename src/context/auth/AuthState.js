import React, { useReducer } from 'react';
import PropTypes from 'prop-types'
import axios from 'axios'
import AuthContext from './authContext';
import AuthReducer from './authReducer';
import setAuthToken from '../../utils/setAuthToken'
import {
    USER_LOADED,
    AUTH_ERROR,
    LOGIN_SUCCESS,
    LOGIN_FAIL,
    LOGOUT,
    CLEAR_ERRORS,
} from '../types';

const AuthState = (props) => {
    const initialState = {
        token: localStorage.getItem('token'),
        client: localStorage.getItem('client'),
        uid: localStorage.getItem('uid'),
        isAuthenticated: false,
        loading: true,
        user: null,
        error: null
    };

    const [state, dispatch] = useReducer(AuthReducer, initialState);

    // Load user: check if the user is logged
    const loadUser = async () => {
        if (localStorage.token) {
            setAuthToken(localStorage.token);
        }

        try {
            const companiesResponse = await axios.post('https://empresas.ioasys.com.br/api/v1/users/auth/sign_in')

            dispatch({
                type: USER_LOADED,
                payload: companiesResponse.data.headers
            })

            // loadUser();

        } catch (err) {
            dispatch({
                type: AUTH_ERROR,
            })
        }
    }

    // Login user
    const loginUser = async (formData) => {

        try {
            const companiesResponse = await axios.post('https://empresas.ioasys.com.br/api/v1/users/auth/sign_in', formData)

            dispatch({
                type: LOGIN_SUCCESS,
                payload: companiesResponse.headers
            })

        } catch (err) {
            dispatch({
                type: LOGIN_FAIL,
                payload: err.response.data.errors[0]
            })
        }
    }

    // Logout
    const logoutUser = () => dispatch({ type: LOGOUT })

    // Clear errors
    const clearErrors = () => dispatch({ type: CLEAR_ERRORS })

    return (
        <AuthContext.Provider
            value={{
                token: state.token,
                client: state.client,
                uid: state.uid,
                isAuthenticated: state.isAuthenticated,
                loading: state.loading,
                user: state.user,
                error: state.error,
                loadUser,
                loginUser,
                logoutUser,
                clearErrors
            }}
        >
            {props.children}
        </AuthContext.Provider>
    )
}

AuthState.propTypes = {
    children: PropTypes.object.isRequired
}

export default AuthState;