import {
    USER_LOADED,
    AUTH_ERROR,
    LOGIN_SUCCESS,
    LOGIN_FAIL,
    LOGOUT,
    CLEAR_ERRORS,
} from '../types';

export default (state, action) => {
    switch (action.type) {
        case USER_LOADED:
            return {
                ...state,
                isAuthenticated: true,
                loading: false,
                uid: action.payload,
                token: localStorage.getItem('token'),
                client: null,
            }
        case LOGIN_SUCCESS:
            localStorage.setItem('token', action.payload['access-token']);
            localStorage.setItem('uid', action.payload['uid']);
            localStorage.setItem('client', action.payload['client']);
            return {
                ...state,
                isAuthenticated: true,
                loading: false,
            }
        case AUTH_ERROR:
        case LOGIN_FAIL:
        case LOGOUT:
            localStorage.removeItem('token');
            localStorage.removeItem('uid');
            localStorage.removeItem('client');
            return {
                ...state,
                token: null,
                uid: null,
                client: null,
                isAuthenticated: false,
                loading: false,
                error: action.payload
            }
        case CLEAR_ERRORS:
            return {
                ...state,
                error: null
            }
        default:
            // return state;
            throw Error(`Unhandled type: ${action.type}, ${action.payload}`);
    }
}